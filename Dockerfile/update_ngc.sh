#/bin/sh
docker build . -t nvcr.io/nvidian/sae/tf2-ngc:latest -t jaycase/tf2-ngc:latest --network host --no-cache
docker push nvcr.io/nvidian/sae/tf2-ngc:latest
docker push jaycase/tf2-ngc:latest
