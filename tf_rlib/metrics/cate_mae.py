import numpy as np
import tensorflow as tf
from tensorflow.python.keras.metrics import MeanMetricWrapper


class CategoricalMAE(MeanMetricWrapper):
    def __init__(self, name='CategoricalMAE', dtype=None):
        super(CategoricalMAE, self).__init__(self.mae, name, dtype=dtype)

    def mae(self, y_true, y_pred):
        y_true = tf.math.argmax(y_true, axis=-1)
        y_pred = tf.math.argmax(y_pred, axis=-1)
        ret = tf.math.abs(y_true - y_pred)
        return ret
