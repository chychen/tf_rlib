import numpy as np
import tensorflow as tf
from tensorflow.python.keras.metrics import MeanMetricWrapper


class MeanAbsolutePercentageError(MeanMetricWrapper):
    """ we customize it because most of the time, our output are normalized, therefore we need to scale it back before calculating the metrics.
    `loss = 100 * abs(y_true - y_pred) / y_true`
    """
    def __init__(self,
                 name='MeanAbsolutePercentageError',
                 dtype=None,
                 denorm_fn=None):
        super(MeanAbsolutePercentageError, self).__init__(self.mape(denorm_fn),
                                                          name,
                                                          dtype=dtype)

    def mape(self, denorm_fn):
        if denorm_fn is None:
            denorm_fn = lambda x: x
        def func(y_true, y_pred):
            rank = len(y_true.shape)
            denorm_y_true = denorm_fn(y_true)
            denorm_y_pred = denorm_fn(y_pred)
            # Prevent division by zero, 1e-7>x>=0 -> 1e-7 (the second smallest value in fp16)
            denorm_y_true = tf.where(denorm_y_true >= 0,
                                     tf.maximum(denorm_y_true, 1e-7),
                                     denorm_y_true)
            denorm_y_pred = tf.where(denorm_y_pred >= 0,
                                     tf.maximum(denorm_y_pred, 1e-7),
                                     denorm_y_pred)
            ret = tf.reduce_mean(100. * tf.abs(denorm_y_true - denorm_y_pred) /
                                 denorm_y_true,
                                 axis=np.arange(1, rank))
            return ret

        return func
