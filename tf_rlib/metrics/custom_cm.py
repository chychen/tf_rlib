import tensorflow as tf
from absl import flags, logging
import io
import matplotlib.pyplot as plt
import seaborn as sns
import itertools

FLAGS = flags.FLAGS


class CustomCM(tf.keras.metrics.Metric):
    def __init__(self, name='cm', **kwargs):
        """
        """
        super(CustomCM, self).__init__(name=name, **kwargs)
        self.cm = self.add_weight(name='cm123',
                                  shape=(FLAGS.out_dim, FLAGS.out_dim),
                                  initializer='zeros',
                                  dtype=tf.float32)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_true = tf.math.argmax(y_true, axis=-1)
        y_pred = tf.math.argmax(y_pred, axis=-1)
        tmp_cm = tf.math.confusion_matrix(y_true,
                                          y_pred,
                                          dtype=tf.float32,
                                          num_classes=FLAGS.out_dim)
        self.cm.assign_add(tmp_cm)

    def result(self):
        # norm
        normed_cm = self.cm / (tf.math.reduce_sum(self.cm,
                                                  axis=-1)[:, tf.newaxis])
        normed_cm = normed_cm.numpy()

        figure = plt.figure(figsize=(8, 8))
        plt.imshow(normed_cm, interpolation='nearest', cmap=plt.cm.Blues)
        plt.title("Confusion matrix")
        plt.colorbar()
        class_names = [str(i) for i in range(FLAGS.out_dim)]
        tick_marks = [i for i in range(FLAGS.out_dim)]
        plt.xticks(tick_marks, class_names, rotation=45)
        plt.yticks(tick_marks, class_names)

        threshold = tf.math.reduce_max(normed_cm) / 2.
        for i, j in itertools.product(range(normed_cm.shape[0]),
                                      range(normed_cm.shape[1])):
            color = "white" if normed_cm[i, j] > threshold else "black"
            plt.text(j,
                     i,
                     normed_cm[i, j],
                     horizontalalignment="center",
                     color=color)

        plt.ylabel('True label')
        plt.xlabel('Predicted label')

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        # Closing the figure prevents it from being displayed directly inside the notebook.
        plt.close(figure)
        buf.seek(0)
        # Convert PNG buffer to TF image
        image = tf.image.decode_png(buf.getvalue(), channels=4)
        # Add the batch dimension
        image = tf.expand_dims(image, 0)
        return image

    def reset_states(self):
        self.cm.assign(tf.zeros_like(self.cm))
