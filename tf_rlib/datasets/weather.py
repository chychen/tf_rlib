import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
import os
from tensorflow.keras.utils import to_categorical
from absl import flags, logging
from tqdm.auto import tqdm
from glob import glob
from datetime import datetime
from PIL import Image
from tf_rlib.datasets.augmentation import RandAugment
from tf_rlib import datasets
import h5py
import datetime
from dateutil.relativedelta import relativedelta

FLAGS = flags.FLAGS
LOGGER = logging.get_absl_logger()


class CloudMask(datasets.Dataset):
    def __init__(self, train_ratio=0.7, target_size=(64, 64)):  #TODO
        super(CloudMask, self).__init__()
        self.train_ratio = train_ratio
        self.target_size = target_size
        self.root_dir = '/ws_data/CWB/cloud-mask/'
        self.mask_file = 'clm.npy'
        self.feat_files = ["QCLOUD.npy", "QGRAUP.npy", "QICE.npy", "QRAIN.npy", "QSNOW.npy", "QVAPOR.npy", "T2.npy", "ctt.npy", "rh.npy", "rh2m.npy"]

    def get_data(self):
        # labels==0 : no cloud, ==3 : cloud
        labels = np.load(os.path.join(self.root_dir, self.mask_file))
        labels = tf.image.resize(labels[...,None], self.target_size)
        indices = tf.where(labels[..., 0]<2.5, 0, 1)
        labels = tf.one_hot(indices, 2).numpy().astype(np.float32)
        
        inputs = []
        for feat_file in self.feat_files:
            input_tmp = np.load(os.path.join(self.root_dir, 'feature', feat_file))
            input_tmp = tf.image.resize(input_tmp[...,None], self.target_size)
            inputs.append(input_tmp)
        inputs = np.concatenate(inputs, axis=-1)
        
        num_train = int(self.train_ratio * labels.shape[0])
        
        # Train & Validation split
        train_inputs = inputs[:num_train]
        train_labels = labels[:num_train]
        
        valid_inputs = inputs[num_train:]
        valid_labels = labels[num_train:]
        
        # Normalization
        inputs_mean = np.mean(train_inputs, axis=(0, 1,2))
        inputs_std = np.std(train_inputs, axis=(0, 1,2))
        
        train_inputs = (train_inputs-inputs_mean)/inputs_std
        valid_inputs = (valid_inputs-inputs_mean)/inputs_std
        
        # pre-shuffle
        p = np.random.permutation(len(train_inputs))
        train_inputs = train_inputs[p]
        train_labels = train_labels[p]
        
        train_ds = tf.data.Dataset.from_tensor_slices(
            (train_inputs, train_labels))
        test_ds = tf.data.Dataset.from_tensor_slices(
            (valid_inputs, valid_labels))
        
        @tf.function
        def augmentation(x, y):
            x = tfa.image.random_cutout(x, mask_size=(32, 16))
            x = tfa.image.random_cutout(x, mask_size=(16, 32))
            return x, y
        
        train_ds = train_ds.shuffle(num_train).batch(
            FLAGS.bs, drop_remainder=True).map(augmentation,
            num_parallel_calls=tf.data.experimental.AUTOTUNE).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)
        test_ds = test_ds.cache().batch(
                FLAGS.bs, drop_remainder=False).prefetch(
                    buffer_size=tf.data.experimental.AUTOTUNE)

        LOGGER.info(f'{train_ds}, {test_ds}')
        return train_ds, test_ds

class PM25Test(datasets.Dataset):
    def __init__(self):
        super(PM25Test, self).__init__()
        self.img_shape = (64, 64)
        self.root_dir = f'/ws_data/CWB/pm2.5/h5py/balanced{self.img_shape}/'
        SHARDS_WATERSHED = [0, 10, 20, 30, 40, 55, np.inf]
        NUM_TRAIN_SHARDS = len(SHARDS_WATERSHED)-1
        self.train_files = [h5py.File(os.path.join(self.root_dir, f'train_{SHARDS_WATERSHED[i]}-{SHARDS_WATERSHED[i+1]}.hdf5'), 'r') for i in range(NUM_TRAIN_SHARDS)]
        self.valid_f = h5py.File(os.path.join(self.root_dir, 'valid.hdf5'), 'r')
        
    def get_y_denorm_fn(self):
        return lambda x: x

    def get_data(self):
        
        def generator(file):
            for time in file:
                target_time = datetime.datetime.strptime(time, '%Y%m%d-%H%M%S') + relativedelta(hours=1)
                target_time = target_time.strftime('%Y%m%d-%H%M%S')
                
                try:
                    target_y = file[target_time]['PM2.5'][()]
                    y = file[time]['PM2.5'][()]
                except:
                    continue
                    
                oneday = []
                for k_attr in ['15QCLOUD', '15QRAIN', '15rh', '15ua', '15va', 'z']: # TODO 'lon' lon.shape = (64, 64, 1)
                    tmp = file[time][k_attr][:]
                    oneday.append(tmp)
                oneday = np.stack(oneday, axis=-1)
                yield (oneday, y), target_y-y
        
        def train_generator(idx):
            file = self.train_files[idx]
            return generator(file)
        
        def valid_generator():
            file = self.valid_f
            return generator(file)
        
        output_signature=((tf.TensorSpec(shape=self.img_shape+(15,6), dtype=tf.float32), tf.TensorSpec(shape=(), dtype=tf.float32)), tf.TensorSpec(shape=(), dtype=tf.float32))
        
        train_datasets = [tf.data.Dataset.from_generator(train_generator, args=[f,], output_signature=output_signature).cache().repeat().shuffle(500) for f in range(len(self.train_files))]
        train_dataset = tf.data.experimental.sample_from_datasets(train_datasets, weights=[1./len(train_datasets) for _ in range(len(train_datasets))])
        train_dataset = train_dataset.shuffle(1000).batch(FLAGS.bs).prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

        #         @tf.function
        #         def augmentation(x, y, pad=4):
        #             x = tf.image.resize_with_crop_or_pad(x, 32 + pad * 2, 32 + pad * 2)
        #             x = tf.image.random_crop(x, [32, 32, 1])
        #             return x, y

        valid_dataset = tf.data.Dataset.from_generator(valid_generator, output_signature=output_signature)
        valid_dataset = valid_dataset.cache().batch(
            FLAGS.bs, drop_remainder=False).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)
        return train_dataset, valid_dataset

class IRQPEDataTest(datasets.Dataset):
    def __init__(self):
        super(IRQPEDataTest, self).__init__()
        self.root_dir = '/ws_data/CWB/IRQPE/20210203/'
        self.img_shape = (141, 111)

    def get_y_denorm_fn(self):
        #         return lambda x: tf.exp((tf.clip_by_value(x, -1, 1)+1.)*3.)
        return lambda x: x

    def get_data(self):
        train_x = np.load(os.path.join(self.root_dir, 'train_x_rain.npy'))
        train_y = np.load(os.path.join(self.root_dir, 'train_y_rain_n.npy'))
        #         train_y = np.load(os.path.join(self.root_dir, 'train_y_rain_normed.npy'))
        #         train_y = np.load(os.path.join(self.root_dir, 'train_y_rain_normed_blur2.npy'))
        valid_x = np.load(os.path.join(self.root_dir, 'valid_x.npy'))
        valid_y = np.load(os.path.join(self.root_dir, 'valid_y_n.npy'))
        #         valid_y = np.load(os.path.join(self.root_dir, 'valid_y_normed.npy'))
        # norm
        mean = np.mean(train_x, axis=(0, 1, 2))
        std = np.std(train_x, axis=(0, 1, 2))
        train_x = (train_x - mean) / std
        valid_x = (valid_x - mean) / std

        target_shape = (-1, *self.img_shape, 1)
        train_x = np.reshape(train_x, target_shape)
        valid_x = np.reshape(valid_x, target_shape)
        train_y = np.reshape(train_y, target_shape)
        valid_y = np.reshape(valid_y, target_shape)

        LOGGER.info('mean.shape:{}, std.shape:{}'.format(
            mean.shape, std.shape))

        #         @tf.function
        #         def augmentation(x, y, pad=4):
        #             x = tf.image.resize_with_crop_or_pad(x, 32 + pad * 2, 32 + pad * 2)
        #             x = tf.image.random_crop(x, [32, 32, 1])
        #             return x, y

        train_dataset = tf.data.Dataset.from_tensor_slices((train_x, train_y))
        valid_dataset = tf.data.Dataset.from_tensor_slices((valid_x, valid_y))
        #         .map(augmentation,
        #             num_parallel_calls=tf.data.experimental.AUTOTUNE)
        train_dataset = train_dataset.cache().shuffle(5000).batch(
            FLAGS.bs, drop_remainder=True).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)
        valid_dataset = valid_dataset.cache().batch(
            FLAGS.bs, drop_remainder=False).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)
        return [train_dataset, valid_dataset]


class TyphoonWind(datasets.Dataset):
    def __init__(self, img_size=(250, 250)):  #TODO
        super(TyphoonWind, self).__init__()
        self.img_augment = RandAugment()
        self.img_size = img_size
        self.root_dir = '/ws_data/CWB/typhoon/Image_B13/'
        self._train_files_ds = tf.data.Dataset.list_files(os.path.join(
            self.root_dir, 'train/*/*'),
                                                          shuffle=True)
        self._test_files_ds = tf.data.Dataset.list_files(os.path.join(
            self.root_dir, 'test/*/*'),
                                                         shuffle=True)
        self.num_train = tf.data.experimental.cardinality(
            self._train_files_ds).numpy()
        self.class_names = np.array(
            sorted([
                os.path.basename(item)
                for item in glob(os.path.join(self.root_dir, 'train/*'))
            ]))
        LOGGER.info(f'class_names: {self.class_names}')

    def get_data(self):
        def get_label(file_path):
            parts = tf.strings.split(file_path, os.path.sep)
            one_hot = parts[-2] == self.class_names
            #     return tf.math.argmax(one_hot)
            return one_hot

        def decode_img(img):
            img = tf.image.decode_jpeg(img, channels=3)
            return tf.image.resize(img, self.img_size)

        def process_path(file_path):
            label = get_label(file_path)
            img = tf.io.read_file(file_path)
            img = decode_img(img)
            return img, label

        train_ds = self._train_files_ds.map(
            process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        test_ds = self._test_files_ds.map(
            process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)

        @tf.function(autograph=False)
        def augmentation(x, y):
            def my_numpy_func(x):
                x = Image.fromarray(np.uint8(x))
                x = self.img_augment(x)
                x = tf.keras.preprocessing.image.img_to_array(
                    x, data_format='channels_last', dtype=np.float32)
                return x

            x = tf.numpy_function(my_numpy_func, [x], tf.float32)
            x.set_shape([250, 250, 3])
            # Norm
            x = x / 128.0 - 1
            return x, y

        def norm(x, y):
            x = x / 128.0 - 1
            return x, y

        train_ds = train_ds.map(
            augmentation, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        train_ds = train_ds.shuffle(self.num_train).batch(
            FLAGS.bs, drop_remainder=True).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)
        test_ds = test_ds.map(
            norm,
            num_parallel_calls=tf.data.experimental.AUTOTUNE).cache().batch(
                FLAGS.bs, drop_remainder=False).prefetch(
                    buffer_size=tf.data.experimental.AUTOTUNE)

        return train_ds, test_ds


class DopplerWind(datasets.Dataset):
    def __init__(self, gridsize=32):
        super(DopplerWind, self).__init__()
        self.sub_y = [2, 9, 14 + 2, 14 + 9]
        self.gridsize = gridsize
        self.root = f'/ws_data/CWB/doppler_wind/tfrec_{gridsize}'
        mean_x = np.load('/ws_data/CWB/doppler_wind/mean_x.npy').astype(
            np.float32)  # (16,)
        mean_x = np.concatenate([mean_x,
                                 np.zeros(
                                     (14, ),
                                     dtype=np.float32)])  # (16+14,), 14: mask
        std_x = np.load('/ws_data/CWB/doppler_wind/std_x.npy').astype(
            np.float32)  # (16,)
        std_x = np.concatenate([std_x,
                                np.ones(
                                    (14, ),
                                    dtype=np.float32)])  # (16+14,), 14: mask
        self.tally = {
            'mean_x':
            mean_x,
            'std_x':
            std_x,
            'mean_y':
            np.load('/ws_data/CWB/doppler_wind/mean_y.npy').astype(np.float32),
            'std_y':
            np.load('/ws_data/CWB/doppler_wind/std_y.npy').astype(np.float32)
        }

    def get_data(self):
        return self._get_tf_dsets()

    def get_y_denorm_fn(self):
        return lambda x: x * self.tally['std_y'][..., self.sub_y] + self.tally[
            'mean_y'][..., self.sub_y]

    def _get_tf_dsets(self):
        # Create a dictionary describing the features.
        image_feature_description = {
            'y': tf.io.FixedLenFeature([], tf.string),
            'x': tf.io.FixedLenFeature([], tf.string),
        }

        @tf.function
        def parse(example_proto):
            example = tf.io.parse_single_example(example_proto,
                                                 image_feature_description)
            x = tf.io.decode_raw(example['x'], np.float32)
            x = tf.reshape(x, [self.gridsize, self.gridsize, 16 + 14
                               ])  # NOTE: FIXED # 14: mask
            y = tf.io.decode_raw(example['y'], np.float32)
            y = tf.reshape(y,
                           [self.gridsize, self.gridsize, 28])  # NOTE: FIXED
            # Norm
            x = (x - self.tally['mean_x']) / self.tally['std_x']
            y = (y - self.tally['mean_y']) / self.tally['std_y']

            y = tf.gather(y, self.sub_y, axis=-1)

            if FLAGS.amp:
                x = tf.cast(x, tf.float16)
                y = tf.cast(y, tf.float16)

            tf.debugging.check_numerics(x, 'x_not_valid')
            tf.debugging.check_numerics(y, 'y_not_valid')
            return x, y

        # TODO Augmentation

        def get_tfrec_dset(is_train: bool):
            folder = 'train' if is_train else 'test'
            files = tf.io.matching_files(
                os.path.join(self.root, f'{folder}/*.tfrec'))
            shards = tf.data.Dataset.from_tensor_slices(files)
            shards = shards.shuffle(files.numpy().shape[0])
            shards = shards.interleave(
                lambda x: tf.data.TFRecordDataset(x),
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
                block_length=1,
                #             cycle_length=4, # set by num_parallel_calls
                deterministic=True)
            shards = shards.map(
                parse, num_parallel_calls=tf.data.experimental.AUTOTUNE)
            return shards

        train_dataset = get_tfrec_dset(is_train=True)
        train_dataset = train_dataset.shuffle(50000).batch(
            FLAGS.bs, drop_remainder=True).repeat()
        train_dataset = train_dataset.prefetch(
            buffer_size=tf.data.experimental.AUTOTUNE)

        valid_dataset = get_tfrec_dset(is_train=False)
        valid_dataset = valid_dataset.cache().batch(
            FLAGS.bs, drop_remainder=False).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)

        return train_dataset, valid_dataset


class Precipitation(datasets.Dataset):
    """ # TODO
    radar missing seems == -1
    height missing seems == -32767.0
    1. add satelite data
    2. add radar missing mask to input/output loss
    3. add lat, lon (V)
    4. add elevation (V)
    5. make y categorization
    6. norm y? (V)(X)
    7. focal loss
    8. data unbalanced handling
    """
    def __init__(self, x_shape=(45, 45, 6 + 3 + 3),
                 y_shape=(45, 45,
                          1)):  # 6:x, 3:month/day/hour, 3:height/lon/lat
        super(Precipitation, self).__init__()
        self.x_shape = x_shape
        self.y_shape = y_shape
        self.root = f'/ws_data/CWB/precipitation/raw'

    def get_data(self):
        return self._get_tf_dsets()

    def get_y_denorm_fn(self):
        #         # y = tf.math.tanh(tf.math.log(y+1e-2)/4.)
        #         def denorm_fn(y):
        #              # make sure logits didn't exceed tanh valid range
        #             y = tf.clip_by_value(y, -1+1e-4, 1-1e-4)
        #             return tf.math.exp(tf.math.atanh(y)*4.)-1e-2
        #         return denorm_fn
        return lambda x: x

    def _get_tf_dsets(self):
        # Create a dictionary describing the features.
        image_feature_description = {
            'y': tf.io.FixedLenFeature([], tf.string),
            'x': tf.io.FixedLenFeature([], tf.string),
        }

        @tf.function
        def parse(example_proto):
            example = tf.io.parse_single_example(example_proto,
                                                 image_feature_description)
            x = tf.io.decode_raw(example['x'], np.float32)
            x = tf.reshape(x, self.x_shape)
            y = tf.io.decode_raw(example['y'], np.float32)
            y = tf.reshape(y, self.y_shape)  # NOTE: FIXED
            # Norm
            # X::: 0~5:radar, 6:month, 7:day, 8:hour, 9:height, 10:lon, 11:lat
            radar = x[..., :6]
            radar = tf.math.maximum(
                radar, 0.)  # TODO, add missing mask to input/output loss
            radar = tf.math.tanh(tf.math.log(radar + 1e-2) / 4.)
            month = x[..., 6:7] / 12.
            day = x[..., 7:8] / 31.
            hour = x[..., 8:9] / 24.
            height = tf.math.maximum(x[..., 9:10], 0)
            height = tf.math.tanh(tf.math.log(height + 1e-3) / 3.)
            height_mask = tf.cast(x[..., 9:10] < 0., tf.float32)
            lon = (x[..., 10:11] - 121.9) / (120.025 - 121.9) * 2 - 1.
            lat = (x[..., 11:12] - 21.7) / (25.35 - 21.7) * 2 - 1.
            x = tf.concat(
                [radar, month, day, hour, height, height_mask, lon, lat],
                axis=-1)

            # make sure no missing value
            y = tf.math.maximum(y, 0)
            #             y = tf.math.tanh(tf.math.log(y+1e-2)/4.)

            #             if FLAGS.amp:
            #                 x = tf.cast(x, tf.float16)
            #                 y = tf.cast(y, tf.float16)
            return x, y

        # TODO Augmentation

        def get_tfrec_dset(is_train: bool):
            folder = 'train' if is_train else 'test'
            files = tf.io.matching_files(
                os.path.join(self.root, f'{folder}/*.tfrec'))
            shards = tf.data.Dataset.from_tensor_slices(files)
            shards = shards.shuffle(files.numpy().shape[0])
            shards = shards.interleave(
                lambda x: tf.data.TFRecordDataset(x),
                num_parallel_calls=tf.data.experimental.AUTOTUNE,
                block_length=1,
                #             cycle_length=4, # set by num_parallel_calls
                deterministic=True)
            shards = shards.map(
                parse, num_parallel_calls=tf.data.experimental.AUTOTUNE)
            return shards

        train_tmp = get_tfrec_dset(is_train=True)
        train_filter_list = []
        groups = 10
        stride = 150. / groups
        for i in range(groups - 1):
            train_filter_list.append(
                train_tmp.filter(lambda x, y: tf.reduce_max(y) >=
                                 (i) * stride and tf.reduce_max(y) <
                                 (i + 1) * stride).cache().shuffle(50000 //
                                                                   groups))
        train_filter_list.append(
            train_tmp.filter(lambda x, y: tf.reduce_max(y) >=
                             (groups - 1) * stride).cache().shuffle(50000 //
                                                                    groups))
        for i, dset in enumerate(train_filter_list):
            counter = 0
            for _ in dset:
                counter = counter + 1
            LOGGER.info(f'dataset-{i}: {counter}')
        train_filter_list = [dset.repeat() for dset in train_filter_list]
        train_dataset = tf.data.experimental.sample_from_datasets(
            train_filter_list,
            [1. / groups for _ in range(groups)]).batch(FLAGS.bs,
                                                        drop_remainder=True)

        #         train_dataset = train_dataset.repeat()

        train_dataset = train_dataset.prefetch(
            buffer_size=tf.data.experimental.AUTOTUNE)

        valid_dataset = get_tfrec_dset(is_train=False)
        valid_dataset = valid_dataset.cache().batch(
            FLAGS.bs, drop_remainder=False).prefetch(
                buffer_size=tf.data.experimental.AUTOTUNE)

        return train_dataset, valid_dataset
