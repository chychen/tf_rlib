import tensorflow as tf
import numpy as np
import os
from absl import flags, logging
from tf_rlib import datasets

FLAGS = flags.FLAGS
LOGGER = logging.get_absl_logger()


class DummyData(datasets.Dataset):
    def __init__(self, in_shape, out_shape, num_data=100, in_dtype=np.float32, out_dtype=np.float32, segmentation=False):
        super(DummyData, self).__init__()
        self.in_shape = (num_data,) + in_shape
        self.out_shape = (num_data,) + out_shape 
        self.num_data = num_data
        self.in_dtype = in_dtype
        self.out_dtype = out_dtype
        self.segmentation = segmentation
        
    def get_data(self):
        x = np.random.rand(*self.in_shape)
        if self.segmentation:
            y = np.random.randint(2, size=self.out_shape)
        else:
            y = np.random.rand(*self.out_shape)
        y = y.astype(self.out_dtype)

        train_dataset = tf.data.Dataset.from_tensor_slices(
                    (x, y)).cache().repeat(self.num_data).shuffle(100).batch(
                        FLAGS.bs, drop_remainder=True).prefetch(
                            buffer_size=tf.data.experimental.AUTOTUNE)
        valid_dataset = tf.data.Dataset.from_tensor_slices(
                    (x, y)).cache().repeat(self.num_data).shuffle(100).batch(
                        FLAGS.bs, drop_remainder=True).prefetch(
                            buffer_size=tf.data.experimental.AUTOTUNE)
        return train_dataset, valid_dataset
