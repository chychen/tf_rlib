import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_hub as hub
from tensorflow.keras.mixed_precision import experimental as mixed_precision
from tf_rlib.runners.base import runner
from tf_rlib import metrics
from absl import flags
from absl import logging
import numpy as np

FLAGS = flags.FLAGS


class BiTClassifyRegressionRunner(runner.Runner):
    def __init__(self, train_dataset, valid_dataset=None):
        super(BiTClassifyRegressionRunner,
              self).__init__(train_dataset,
                             valid_dataset=valid_dataset,
                             best_state='auc')

    def init(self):
        # Load model into KerasLayer
        model_url = "https://tfhub.dev/google/bit/m-r50x1/1"
        self.model = tf.keras.Sequential([
            hub.KerasLayer(model_url, trainable=True),
            tf.keras.layers.Dense(
                FLAGS.out_dim,
                kernel_initializer='zeros',
                kernel_regularizer=tf.keras.regularizers.l1_l2(l1=FLAGS.l1,
                                                               l2=FLAGS.l2)),
            tf.keras.layers.Softmax()
        ])

        train_metrics = {
            'loss': tf.keras.metrics.Mean('loss'),
            'acc': tf.keras.metrics.CategoricalAccuracy('acc')
        }
        valid_metrics = {
            'loss':
            tf.keras.metrics.Mean('loss'),
            'acc':
            tf.keras.metrics.CategoricalAccuracy('acc'),
            'auc':
            tf.keras.metrics.AUC(num_thresholds=200),
            'top3acc':
            tf.keras.metrics.TopKCategoricalAccuracy(k=3, name='top3acc'),
            'cm':
            metrics.CustomCM('cm'),
            'categorical_mae':
            metrics.CategoricalMAE('categorical_mae')
        }
        self.loss_object = tf.keras.losses.CategoricalCrossentropy(
            from_logits=False,
            reduction=tf.keras.losses.Reduction.NONE)  # distributed-aware
        self.optim = tfa.optimizers.AdamW(
            weight_decay=0.0,  # will be modified later
            lr=0.0,  # will be modified later
            beta_1=FLAGS.adam_beta_1,
            beta_2=FLAGS.adam_beta_2,
            epsilon=FLAGS.adam_epsilon)
        return {'bit-m': self.model}, None, train_metrics, valid_metrics

    def begin_fit_callback(self, lr):
        self.init_lr = lr
        self.lr_scheduler = tf.keras.experimental.CosineDecay(
            self.init_lr, None)

    def begin_epoch_callback(self, epoch_id, epochs):
        self.log_scalar('lr', self.optim.learning_rate, training=True)
        self.log_scalar('wd', self.optim.weight_decay, training=True)

    def begin_step_callback(self, step_id, epochs):
        if step_id < FLAGS.steps_per_epoch * FLAGS.warmup:
            self.optim.learning_rate = step_id / (FLAGS.steps_per_epoch *
                                                  FLAGS.warmup) * self.init_lr
        else:
            self.lr_scheduler.decay_steps = FLAGS.steps_per_epoch * epochs
            self.optim.learning_rate = self.lr_scheduler(step_id)
        self.optim.weight_decay = FLAGS.wd * self.optim.learning_rate / self.init_lr

    def train_step(self, x, y):
        """
        Args:
            x (batch): mini batch data
            y (batch): mini batch label
        Returns:
            losses (dict)
        """
        with tf.GradientTape() as tape:
            probs = self.model(x, training=True)
            loss = self.loss_object(y, probs)
            loss = tf.nn.compute_average_loss(
                loss, global_batch_size=FLAGS.bs)  # distributed-aware
            total_loss = loss

        grads = tape.gradient(total_loss, self.model.trainable_weights)

        self.optim.apply_gradients(zip(grads, self.model.trainable_weights))
        return {'loss': [loss], 'acc': [y, probs]}

    def validate_step(self, x, y):
        """
        Args:
            x (batch): mini batch data
            y (batch): mini batch label
        Returns:
            metrics (dict)
        """
        probs = self.model(x, training=False)
        loss = self.loss_object(y, probs)
        loss = tf.nn.compute_average_loss(loss, global_batch_size=FLAGS.bs)
        return {
            'loss': [loss],
            'acc': [y, probs],
            'auc': [y, probs],
            'top3acc': [y, probs],
            'cm': [y, probs],
            'categorical_mae': [y, probs]
        }

    @property
    def required_flags(self):
        return ['out_dim', 'bs', 'steps_per_epoch']

    @property
    def support_amp(self):
        return False
