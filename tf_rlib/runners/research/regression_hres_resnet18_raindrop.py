import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow.keras.mixed_precision import experimental as mixed_precision
from tf_rlib.models.research import HResResNet18
from tf_rlib.runners.base import runner
from tf_rlib import metrics
from tf_rlib import losses
from absl import flags
from absl import logging
import numpy as np

FLAGS = flags.FLAGS
LOGGER = logging.get_absl_logger()


class RegHResResNet18RunnerRaindrop(runner.Runner):
    """ 
    Regression High Resolution ResNet18 Runner
    """
    def __init__(self, train_dataset, valid_dataset=None, y_denorm_fn=None):
        self.y_denorm_fn = y_denorm_fn
        super(RegHResResNet18RunnerRaindrop,
              self).__init__(train_dataset,
                             valid_dataset=valid_dataset,
                             best_state='rmse')

    def init(self):
        self.model = HResResNet18(preact=True)
        train_metrics = {
            'loss':
            tf.keras.metrics.MeanTensor('loss'),
            'rmse':
            metrics.RootMeanSquaredError('rmse', denorm_fn=self.y_denorm_fn),
            'mae':
            metrics.MeanAbsoluteError('mae', denorm_fn=self.y_denorm_fn),
            'mape':
            metrics.MeanAbsolutePercentageError('mape',
                                                denorm_fn=self.y_denorm_fn)
        }
        valid_metrics = {
            'loss':
            tf.keras.metrics.MeanTensor('loss'),
            'rmse':
            metrics.RootMeanSquaredError('rmse', denorm_fn=self.y_denorm_fn),
            'mae':
            metrics.MeanAbsoluteError('mae', denorm_fn=self.y_denorm_fn),
            'mape':
            metrics.MeanAbsolutePercentageError('mape',
                                                denorm_fn=self.y_denorm_fn)
        }
        self.optim = tfa.optimizers.AdamW(weight_decay=0.0,
                                          learning_rate=0.0,
                                          beta_1=FLAGS.adam_beta_1,
                                          beta_2=FLAGS.adam_beta_2,
                                          epsilon=FLAGS.adam_epsilon)
        #         self.optim = tfa.optimizers.SGDW(weight_decay=0.0,
        #                                          learning_rate=0.0,
        #                                          momentum=0.9,
        #                                          nesterov=True)
        if FLAGS.amp:
            self.optim = mixed_precision.LossScaleOptimizer(
                self.optim, loss_scale='dynamic')
        return {'HResResNet18': self.model}, None, train_metrics, valid_metrics

    def begin_fit_callback(self, lr):
        self.init_lr = lr
        self.lr_scheduler = tf.keras.experimental.CosineDecay(
            self.init_lr, None)

    def begin_epoch_callback(self, epoch_id, epochs):
        self.log_scalar('lr', self.optim.learning_rate, training=True)
        self.log_scalar('wd', self.optim.weight_decay, training=True)

    def begin_step_callback(self, step_id, epochs):
        if step_id < FLAGS.steps_per_epoch * FLAGS.warmup:
            self.optim.learning_rate = step_id / (FLAGS.steps_per_epoch *
                                                  FLAGS.warmup) * self.init_lr
        else:
            self.lr_scheduler.decay_steps = FLAGS.steps_per_epoch * epochs
            self.optim.learning_rate = self.lr_scheduler(step_id)
        self.optim.weight_decay = FLAGS.wd * self.optim.learning_rate / self.init_lr

    def train_step(self, x, y):
        """
        """
        with tf.GradientTape() as tape:
            logits = self.model(x, training=True)
            #             logits = tf.nn.relu(logits)
            if FLAGS.loss_fn == 'mse':
                loss = self.mse(y, logits)
            elif FLAGS.loss_fn == 'mae':
                loss = self.mae(y, logits)
            else:
                raise NotImplementedError
            loss = tf.nn.compute_average_loss(
                loss, global_batch_size=FLAGS.bs)  # distributed-aware
            regularization_loss = tf.nn.scale_regularization_loss(
                tf.math.add_n(self.model.losses))  # distributed-aware
            if FLAGS.amp:
                regularization_loss = tf.cast(regularization_loss, tf.float16)
                total_loss = loss + regularization_loss
                total_loss = self.optim.get_scaled_loss(total_loss)
            else:
                total_loss = loss + regularization_loss

        if FLAGS.amp:
            grads = tape.gradient(total_loss, self.model.trainable_weights)
            grads = self.optim.get_unscaled_gradients(grads)
        else:
            grads = tape.gradient(total_loss, self.model.trainable_weights)

        self.optim.apply_gradients(zip(grads, self.model.trainable_weights))
        return {
            'loss': [loss],
            'rmse': [y, logits],
            'mae': [y, logits],
            'mape': [y, logits]
        }

    def validate_step(self, x, y):
        """
        Args:
            x (batch): mini batch data
            y (batch): mini batch label
        Returns:
            metrics (dict)
        """

        logits = self.model(x, training=False)
        #         logits = tf.nn.relu(logits)
        if FLAGS.loss_fn == 'mse':
            loss = self.mse(y, logits)
        elif FLAGS.loss_fn == 'mae':
            loss = self.mae(y, logits)
        else:
            raise NotImplementedError
        loss = tf.nn.compute_average_loss(
            loss, global_batch_size=FLAGS.bs)  # distributed-aware

        return {
            'loss': [loss],
            'rmse': [y, logits],
            'mae': [y, logits],
            'mape': [y, logits]
        }

    @tf.function
    def inference(self, x):
        logits = self.model(x, training=False)
        #         logits = tf.nn.relu(logits)
        return logits

    @property
    def required_flags(self):
        return ['dim', 'out_dim', 'bs', 'loss_fn']

    @property
    def support_amp(self):
        return True

    def mse(self, y, logits):
        if logits.dtype == tf.float16:
            logits = tf.cast(logits, tf.float32)
        if y.dtype == tf.float16:
            y = tf.cast(y, tf.float32)
        loss = tf.reduce_mean((y - logits)**2, axis=(1, 2, 3))
        if FLAGS.amp and loss.dtype == tf.float32:
            loss = tf.cast(loss, tf.float16)
        return loss

    def mae(self, y, logits):
        return tf.reduce_mean(tf.abs(y - logits), axis=(1, 2, 3))

    def custom_log_data(self, x_batch, y_batch):
        logits = self.model(x_batch, training=False)
        #         logits = tf.nn.relu(logits)
        ret_dict = {'logits': logits}
        return ret_dict
