# python dev_precipitation.py --gpus="0,1" --exp_name="baseline"
import sys

sys.path.append('../..')
import tf_rlib
from tf_rlib.runners.research import RegHResResNet18RunnerRaindrop

FLAGS = tf_rlib.FLAGS

# tf_rlib.utils.purge_logs()
# tf_rlib.utils.set_exp_name('RegHResResNet18RunnerRaindrop_balanced10')
# tf_rlib.utils.set_gpus('0,1,2,3')
tf_rlib.utils.set_logging('INFO')
tf_rlib.utils.set_amp(False)
amp_factor = 2 if FLAGS.amp else 1

FLAGS.loss_fn = 'mse'
FLAGS.bs = 64 * amp_factor * FLAGS.num_gpus
FLAGS.in_rank = 2
FLAGS.out_dim = 1
FLAGS.lr = 5e-4 * amp_factor * FLAGS.num_gpus
FLAGS.steps_per_epoch = 20000 // FLAGS.bs
FLAGS.epochs = 300
FLAGS.conv_act = 'leakyrelu'
FLAGS.wd = 0.
FLAGS.conv_norm = 'GroupNormalization'
FLAGS.groups = 32

Precipitation = tf_rlib.datasets.Precipitation()
train_dset, valid_dset = Precipitation.get_data()
runner = RegHResResNet18RunnerRaindrop(
    train_dset, valid_dset, y_denorm_fn=Precipitation.get_y_denorm_fn())
runner.fit(FLAGS.epochs, lr=FLAGS.lr)
