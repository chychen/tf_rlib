## TODO ResNet
import sys
sys.path.append('../..')
import tf_rlib
from tf_rlib.runners import RegressionRunner
from tf_rlib.datasets import PM25Test
import numpy as np
import tensorflow as tf
FLAGS = tf_rlib.FLAGS

# tf_rlib.utils.purge_logs()
tf_rlib.utils.set_exp_name('RNet-3D-bias-Concat-1hr')
tf_rlib.utils.set_gpus('0')
tf_rlib.utils.set_logging('INFO')
tf_rlib.utils.set_amp(False)

FLAGS.steps_per_epoch=50
FLAGS.bs=32
FLAGS.in_rank=3
FLAGS.out_dim=1

# # PyramidNet
# FLAGS.depth=18
# FLAGS.bottleneck=False
# layers_per_block = 3 if FLAGS.bottleneck else 2
# total_blocks=(FLAGS.depth-2)/layers_per_block

FLAGS.lr=1e-4
FLAGS.epochs=300
FLAGS.loss_fn='mse'

dsets = PM25Test().get_data()
runner = RegressionRunner(*dsets)
runner.fit(FLAGS.epochs, lr=FLAGS.lr)